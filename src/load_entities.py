from config.config_dow_jones import avro_field_names

def load_entities_from_article(article):
    try:
        entities = article['entities']
        return entities
    except KeyError:
        print('Pas d\'entités dans l\'article {}'.format(article[avro_field_names['id']]))
        return {}