from utils.utils_for_string import to_standard
from utils.path_generation import get_input_path, get_output_data_path
from utils.save_articles import save_updated_article
import os
import datetime
import re


def get_source_from_article_id(article_id):
    """
    Return the normalize source code
    :param article_id: article id as defined in step1
    :return: LMP source code as defined in the config source file
    """
    lmp_source_code = article_id.split('_')[0]
    return lmp_source_code


class Corpus:
    def __init__(self, dict_articles):
        self.dict_articles = dict_articles

    def filter_by_topic(self, list_of_words, list_to_exclude=['']):
        """

        :param list_of_words: if empty, keep all articles, otherwise, keep only articles containing the correct words, which are not in list_to_exclude
        :param list_to_exclude: list of regex that are removed from the title, in order to prevent from false positive. exemple : we want 'eau' in the title, so 'eau'
        is in list_of_words, but we don't want 'tombe à l'eau', so the regex r"tomb[a-z]{1,5} à l.eau" is in list_to_exclude
        :return: articles with at least one words from list_of_words in their title, the regex in list_to_exclude not included in the occurrence of the words from list_of_words
        """
        cleaned_dict_articles = {}
        for id in self.dict_articles:
            article = self.dict_articles[id]
            for to_exclude in list_to_exclude:
                new_titre = re.sub(to_exclude.lower(), '', (article['titre'].lower()))
                if len(list_of_words) > 0:
                    for word in list_of_words:
                        if to_standard(word) in to_standard(new_titre):
                            cleaned_dict_articles[id] = self.dict_articles[id]
                else:
                    cleaned_dict_articles[id] = self.dict_articles[id]
        self.dict_articles = cleaned_dict_articles

    def filter_by_date(self, source_normalizer, date_min, date_max):
        source_date_field = source_normalizer.map_lmp_to_source_field('date')
        self.dict_articles = {c: self.dict_articles[c] for c in self.dict_articles if
                              date_min <= datetime.datetime.utcfromtimestamp(
                                  self.dict_articles[c][
                                      source_date_field] / 1000).date() and date_max >= datetime.datetime.utcfromtimestamp(
                                  self.dict_articles[c][source_date_field] / 1000).date()}

    def filter_by_location(self, list_loc_of_interest):
        cleaned_dict_articles = {}
        set_loc_of_interest_norm = set([to_standard(loc) for loc in list_loc_of_interest])
        for id in self.dict_articles:
            try:
                set_loc_article_norm = set(
                    [to_standard(loc) for loc in self.dict_articles[id]['loc']])
                if len(set_loc_article_norm.intersection(set_loc_of_interest_norm)) > 0:
                    cleaned_dict_articles[id] = self.dict_articles[id]
            except KeyError:
                print('Erreur de clé for article %s mais ok' % id)
        self.dict_articles = cleaned_dict_articles

    def remove_sport(self, remove_sport=True):
        if remove_sport:
            self.dict_articles = {c: self.dict_articles[c] for c in self.dict_articles if
                                  ("sujet" in self.dict_articles[c].keys() and
                                   'gspo' not in self.dict_articles[c]["sujet"]) or (
                                              "sujet" not in self.dict_articles[c].keys())}

    def add_topic(self, topics):
        """
        :param topics: {topic_1: {to_include:[], to_exclude:[]}, topic_2: {to_include:[], to_exclude:[]}...}
        :return:
        """
        for id_article in self.dict_articles:
            self.dict_articles[id_article]['themes'] = []
            for topic in topics:
                new_body = self.dict_articles[id_article]['corps']
                for regex in topics[topic]['to_exclude']:
                    new_body = re.sub(regex.lower(), '', (new_body.lower()))
                for word in topics[topic]['to_include']:
                    if to_standard(word, keep_spaces=True) in to_standard(new_body, keep_spaces=True):
                        self.dict_articles[id_article]['themes'].append(topic)
                        break

    def process_corpus(self, remove_sport, filter_by_location, add_topic=False):
        params = locals()
        if remove_sport:
            print('remove sport')
            self.remove_sport()
        if filter_by_location:
            print('filter by location')
            loc = open(get_input_path('list_locations_of_interest.txt'), 'r')
            list_loc_of_interest = [line.rstrip(os.linesep) for line in loc]
            self.filter_by_location(list_loc_of_interest)
        if add_topic:
            print('add topic')
            from input.topics import topics
            self.add_topic(topics)

    def save(self):
        for id_article in self.dict_articles:
            save_updated_article(self.dict_articles[id_article],
                                 get_output_data_path(get_source_from_article_id(article_id=id_article)),
                                 "%s.json" % id_article)
