import json
import os
from datetime import date


def load_json(file_path):
    with open(file_path, 'r') as f:
        return json.load(f)


def get_articles_path(path_corpus, sources, date_min='1000/01', date_max='3000/01'):
    """
    :param path: contient l'architecture suivante : nom_source/date/article_id
    :param source: nom_source : liste de noms de titres de presse, au format suivant exemple: 'LAMONT' pour La Montagne.
    :param date_min: date min d'intérêt, au format '2008/05'
    :param date_max: date max d'intérêt, ''''
    :return:
    """
    corpus_path = []
    for path, subdirs, files in os.walk(path_corpus):
        for name in [f for f in files if f[0] != '.']:
            new_path = os.path.join(path, name)
            y_min = date_min.split('/')[0]
            m_min = date_min.split('/')[1]
            y_max = date_max.split('/')[0]
            m_max = date_max.split('/')[1]
            y = new_path.split('/')[-3]
            m = new_path.split('/')[-2]
            source = new_path.split('/')[-4]
            if date(year=int(y_min), month=int(m_min), day=1) <= date(int(y), int(m), 1) and date(int(y_max), int(m_max),
                                                                                                 1) >= date(
                    int(y), int(m), 1):
                if sources:
                    if source in sources:
                        corpus_path.append(new_path)
                else:
                    corpus_path.append(new_path)
    return corpus_path


def get_articles(corpus_path):
    corpus = {}
    for file in corpus_path:
        # the key of the article is the LMP key
        corpus[os.path.basename(file)] = load_json(file)
        corpus[os.path.basename(file)]['id'] = os.path.basename(file)
    return corpus


def load_corpus(path, sources, date_min_str='1000/01', date_max_str='3000/12'):
    """
    the format of the date should be YYYY/MM
    :param path:
    :param sources:
    :param date_min_str:
    :param date_max_str:
    :return:
    """
    corpus_path = get_articles_path(path, sources, date_min_str, date_max_str)
    corpus = get_articles(corpus_path)
    return corpus