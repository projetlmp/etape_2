import json
import os
from utils.path_generation import get_preprocess_path
from src.custom_class.corpus import Corpus
from utils.path_generation import get_input_path
from src.custom_class.entities import EntitySet

def load_json(file_path):
    with open(file_path, 'r') as f:
        return json.load(f)


def get_articles_path(path):
    corpus_path = []
    for path, subdirs, files in os.walk(path):
        for name in files:
            corpus_path.append(os.path.join(path, name))
    return corpus_path


def get_articles(corpus_path):
    corpus = {}
    for file in corpus_path:
        corpus[file.split('/')[-1]] = load_json(file)
    return corpus


if __name__ == '__main__':
    pass
