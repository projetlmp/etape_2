import unittest
from utils.path_generation import get_preprocess_path
from src.load_updated_corpus import load_corpus
from src.custom_class.corpus import Corpus


class TestConstructCorpus(unittest.TestCase):
    def setUp(self):
        self.corpus = Corpus(load_corpus(get_preprocess_path("test_corpus"), sources=['ESTREP'], date_min_str='2011/01',
                                  date_max_str='2018/06'))

    def test_construct_corpus(self):
        self.assertEqual(list(self.corpus.dict_articles.keys()),
                         ['ESTREP_02122015_87886191.json', 'ESTREP_09122015_87580277.json',
                          'ESTREP_02122015_46015080.json', 'ESTREP_13122015_46696000.json',
                          'ESTREP_10122015_60201014.json', 'ESTREP_09122015_9536060.json',
                          'ESTREP_03122015_7794141.json', 'ESTREP_12122015_63092096.json',
                          'ESTREP_14122015_86617975.json', 'ESTREP_27122015_26856501.json',
                          'ESTREP_26122015_80574409.json', 'ESTREP_18122015_74784095.json',
                          'ESTREP_30122015_62108479.json', 'ESTREP_06122015_28288477.json',
                          'ESTREP_12122015_98379191.json', 'ESTREP_30032015_10862683.json',
                          'ESTREP_26032015_74104242.json', 'ESTREP_21032015_92677529.json',
                          'ESTREP_29032015_92545464.json', 'ESTREP_07032015_78618565.json',
                          'ESTREP_23032015_87886156.json', 'ESTREP_03032015_19534846.json',
                          'ESTREP_19032015_98031754.json', 'ESTREP_21032015_72396544.json',
                          'ESTREP_24032015_72289593.json', 'ESTREP_12032015_68777477.json',
                          'ESTREP_06032015_56837235.json', 'ESTREP_23032015_3179938.json',
                          'ESTREP_05032015_60484059.json', 'ESTREP_03032015_68630640.json',
                          'ESTREP_04032015_35891820.json', 'ESTREP_08032015_13027164.json',
                          'ESTREP_04032015_42417225.json', 'ESTREP_14032015_19654043.json',
                          'ESTREP_06032015_59733065.json', 'ESTREP_09032015_8301168.json']
                         )
        self.assertEqual(list(self.corpus.dict_articles['ESTREP_09122015_87580277.json'].keys()), ['source_code', 'id', 'sujet', 'date', 'corps', 'titre', 'entities', 'loc'])


