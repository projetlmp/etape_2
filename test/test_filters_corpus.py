import unittest
from utils.path_generation import get_preprocess_path
from src.load_updated_corpus import load_corpus
from src.custom_class.corpus import Corpus


class TestFilterCorpusLoc(unittest.TestCase):
    def setUp(self):
        self.corpus = Corpus(load_corpus(get_preprocess_path("test_corpus"), sources=['ESTREP'], date_min_str='2015/03',
                                         date_max_str='2015/04'))

    def test_corpus_filter(self):
        self.assertEqual(list(self.corpus.dict_articles.keys()), ['ESTREP_30032015_10862683.json', 'ESTREP_26032015_74104242.json',
                                                    'ESTREP_21032015_92677529.json', 'ESTREP_29032015_92545464.json',
                                                    'ESTREP_07032015_78618565.json', 'ESTREP_23032015_87886156.json',
                                                    'ESTREP_03032015_19534846.json', 'ESTREP_19032015_98031754.json',
                                                    'ESTREP_21032015_72396544.json', 'ESTREP_24032015_72289593.json',
                                                    'ESTREP_12032015_68777477.json', 'ESTREP_06032015_56837235.json',
                                                    'ESTREP_23032015_3179938.json', 'ESTREP_05032015_60484059.json',
                                                    'ESTREP_03032015_68630640.json', 'ESTREP_04032015_35891820.json',
                                                    'ESTREP_08032015_13027164.json', 'ESTREP_04032015_42417225.json',
                                                    'ESTREP_14032015_19654043.json', 'ESTREP_06032015_59733065.json',
                                                    'ESTREP_09032015_8301168.json'])


class TestFilterCorpusLoc(unittest.TestCase):
    def setUp(self):
        self.corpus = Corpus(dict_articles={'ESTREP_02122015_87886191.json': {'source_code': 'ESTREP', 'id': 'ESTREP_02122015_87886191.json',
                                               'date': 1449010800000,
                                               'corps': 'L’incendie du corps de ferme qui a coûté la vie à un homme de 62 ans dans la nuit de samedi à dimanche, à Ribeaucourt (notre édition de lundi), est bien d’origine accidentelle, comme le confirment les analyses des techniciens en investigation criminelle de la gendarmerie. Le sexagénaire venait de prendre sa retraite d’une entreprise de travaux publics, située dans la commune. Il habitait seul dans sa maison depuis une quinzaine d’années. Il avait deux enfants, une fille de 29 ans qui vit à Brest, un fils de 20 ans domicilié avec son ex-épouse dans la Drôme. Le soir du drame, le malheureux s’était rendu chez sa mère, domiciliée dans le village, comme il avait coutume de le faire pour dîner. Les éléments de l’enquête indiquent que le dispositif de chauffage au bois de l’habitation était vétuste et que l’insert n’était pas entretenu. « On est sûr que la mort est consécutive à l’incendie et qu’elle résulte d’une inhalation de fumée toxique », informe le parquet de Bar-le-Duc. Découvert dimanche vers 3 h 20 dans les décombres, le corps du défunt a été transporté à l’hôpital de Verdun pour y être examiné par le médecin légiste. Il est toujours dans l’attente d’une identification formelle par le biais d’une analyse ADN confiée à l’Institut de recherche criminelle de la gendarmerie nationale, à Rosny-sous-Bois.',
                                               'titre': 'Meuse : la thèse accidentelle confirmée dans l’incendie mortel de Ribeaucourt',
                                               'entities': {'institutderecherchecriminelledelagendarmerienationale': {
                                                   'name': [
                                                       'Institut de recherche criminelle de la gendarmerie nationale'],
                                                   'type': ['ORG']}}, 'loc': {'ribeaucourt': {'name': ['Ribeaucourt']},
                                                                              'brest': {'name': ['Brest']},
                                                                              'drome': {'name': ['Drôme']},
                                                                              'barleduc': {'name': ['Bar-le-Duc']},
                                                                              'verdun': {'name': ['Verdun']},
                                                                              'rosnysousbois': {
                                                                                  'name': ['Rosny-sous-Bois']}}},
             'ESTREP_09122015_87580277.json': {'source_code': 'ESTREP', 'id': 'ESTREP_09122015_87580277.json',
                                               'sujet': 'gspo', 'date': 1449615600000,
                                               'corps': 'Les écoliers, les parents d’élèves et l’équipe éducative s’attellent depuis les vacances d’automne à la préparation du traditionnel marché de Noël de l’école. Des parents bénévoles ont consacré 2 à 3 après-midi par semaine pour confectionner divers supports destinés à être décorés par les enfants. C’est ainsi que les écoliers ont pris plaisir à réaliser, dans l’esprit de Noël une multitude de petits sujets : objets peints, décorés de paillettes, de perles (pommes de pin, petits pots…) Autant d’objets décoratifs qui prendront place sur le sapin ou la table de fête le soir de Noël. Tandis que certains parents se rendaient disponibles pour encadrer les enfants au sein de petits ateliers de bricolage, d’autres participaient en fournissant des matériaux ou en confectionnant à la maison de petits sujets destinés à la vente. Travail de motricité fine pour les tout-petits (découpage, collage, peinture), éveil aux arts plastiques, acquisition du langage autour de l’évocation des fêtes, coopération, ouverture de l’école, implication des familles, rencontre conviviale entre parents… Autant d’objectifs réalisés par cette action. Les écoliers, les enseignants et les parents invitent, le vendredi 11 décembre à 18 h, au marché de Noël organisé dans les locaux de l’école et dont la recette est destinée à financer des projets scolaires. Les enfants accueilleront le père Noël avec les jolis chants de Noël qu’ils ont appris en classe. Suivra une dégustation de gâteaux, cakes et autres pâtisseries, accompagnés de boissons et du traditionnel vin chaud. Marché de Noël à l’école d’Houdelaincourt, vendredi 11 décembre,à partir de 18 h.',
                                               'titre': 'Les écoliers préparent le marché de Noël', 'entities': {
                     'noel': {'name': ['Noël'], 'type': ['OTHER', 'OTHER', 'OTHER', 'OTHER', 'OTHER', 'OTHER']},
                     'travail': {'name': ['Travail'], 'type': ['ORG']},
                     'marchedenoel': {'name': ['Marché de Noël'], 'type': ['OTHER']}},
                                               'loc': {'houdelaincourt': {'name': ['Houdelaincourt']}}}})




    def test_corpus_sport_filter(self):
        self.corpus.process_corpus(filter_by_location=False, remove_sport=True, add_topic=False)
        self.assertEqual(list(self.corpus.dict_articles.keys()), ['ESTREP_02122015_87886191.json'])

