import sys
sys.path.append('./../')

from input.topic_config import list_of_words_to_exclude, list_of_words_topic
from src.load_corpus import load_json
from src.custom_class.corpus import Corpus
from src.load_updated_corpus import load_corpus

from utils.path_generation import get_input_data_path


def run_etape_2(source_list = None, filter_by_loc=False, filter_by_topic=False):
    article_base_path = get_input_data_path()
    try:
        sources = source_list.split(',')
    except AttributeError:
        sources = None
    corpus = Corpus(dict_articles=load_corpus(article_base_path, sources=sources,
                                              date_min_str='1000/01',
                                              date_max_str='3000/01'))

    corpus.process_corpus(filter_by_location=filter_by_loc,
                          filter_by_topic=filter_by_topic,
                          lst_of_wrds_to_exclude=list_of_words_to_exclude,
                          lst_of_wrds_to_include=list_of_words_topic)
    corpus.save()


if __name__ == '__main__':
    run_etape_2()