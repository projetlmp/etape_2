from config.config_dow_jones import avro_field_names

dic_origin_to_source_dic_col = {
    'DJ': 'DJ Source Code'
}

dic_to_dic_source_fields = {
    'DJ': avro_field_names
}