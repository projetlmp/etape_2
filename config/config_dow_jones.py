# coding: utf-8

avro_field_names = {
    "corps": "body",
    "id_article": "an",
    "titre": "title",
    "date de publication": "publication_datetime",
    "sujet de l'article": "subject_codes",
    "source": "source_name",
    "source_code": "source_code",
    "date": "publication_datetime",

}
