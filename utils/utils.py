from config.config_dow_jones import avro_field_names
import json
import os
import datetime
from datetime import time


def get_list_from_file(filepath):
    f = open(filepath, 'r')
    list_of_words = [line.rstrip(os.linesep) for line in f]
    f.close()
    return list_of_words


class SetEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        if isinstance(obj, datetime.datetime):
            return obj.isoformat()
        if isinstance(obj, time):
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)
