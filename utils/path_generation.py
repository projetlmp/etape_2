import os
from config.path import path_input_data, path_output_data, PROJECT_PATH


def get_preprocess_path(filename):
    return os.path.join(PROJECT_PATH, 'preprocess', filename)


def get_input_path(filename):
    return os.path.join(PROJECT_PATH, 'input', filename)


def get_output_path(filename):
    return os.path.join(path_output_data, filename)

def get_input_data_path():
    return path_input_data

