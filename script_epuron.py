from src.custom_class.corpus import Corpus
from src.load_updated_corpus import load_corpus
from config.path import path_input

if __name__ == '__main__':
    sources = None
    corpus = Corpus(
        dict_articles=load_corpus(path_input, sources=sources,
                                  date_min_str='1000/01',
                                  date_max_str='3000/01'))
    corpus.process_corpus(filter_by_location=False,
                          remove_sport=True,
                          add_topic=True, topics={
            'Eoliennes': {'to_include': ['eolien', 'epuron', 'hydrolienne', 'EDPR'], 'to_exclude': ['']},
            'Agriculture': {
                'to_include': ['agriculture', 'aquaculture', 'arboriculture', 'oléiculture', 'sylviculture', 'agricole',
                               'horticulture', 'élevage', 'agriculteurs', 'ovin', 'bovin', 'agronomique',
                               'agroécologique', 'paysan', 'agrobiologique', "‘agro-industrie’", 'ter'], 'to_exclude': ['']},
            'Energies vertes': {'to_include': ['energies vertes', 'energie verte', 'énergie propre', 'énergie solaire',
                                               'énergie hydroélectrique'],
                                'to_exclude': ['']}, 'Environnement': {
                'to_include': ['biodiversité', 'environnement', 'associations environnementales',
                               'association environnementale', 'protection de l’environnement'], 'to_exclude': ['']},
            'Bure': {
                'to_include': ['enfouissement des déchets nucléaires', "l'Andra", 'Cigeo', 'déchets radioactifs'],
                'to_exclude': ['']}, 'Revendications': {
                'to_include': ['manifestants', 'opposition', 'tractage', 'manifestation contre',
                               'manifestations contre',
                               'manifestation des opposants', 'militant', 'zad'], 'to_exclude': ['']},
            'Energies': {'to_include': ['nucléaire', 'EDF', 'Areva', 'lignes à haute tension', 'Enedis'],
                         'to_exclude': ['']}})
