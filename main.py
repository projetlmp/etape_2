from src.custom_class.corpus import Corpus
from src.load_updated_corpus import load_corpus
from utils.path_generation import get_input_data_path


if __name__ == '__main__':
    sources = None
    filter_by_loc = False
    corpus = Corpus(dict_articles=load_corpus(get_input_data_path(), sources=sources,
                                              date_min_str='1000/01',
                                              date_max_str='3000/01'))

    corpus.process_corpus(filter_by_location=filter_by_loc,
                          remove_sport=True,
                          add_topic=True, topics={
            'eoliennes': {'to_include': ['eolienne', 'parcs eoliens', 'parc eolien'], 'to_exclude': ['']},
            'environnement': {'to_include': ['environnement', 'energie verte', 'energies vertes'], 'to_exclude': ['']},
            'revendications': {
                'to_include': ['associations enironnementales', 'association environnementale', 'manifestation'],
                'to_exclude': ['']}}
                          )
    corpus.save()
