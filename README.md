# Influenceurs - Etape 2

Entrée : corpus d'articles de presse après [l'étape 1](https://bitbucket.org/Marjolaine_Grunenberger/etape_1/src/7ec17780db4a?at=master&mode=edit&spa=0&fileviewer=file-view-default) 

Sortie : ce même corpus qui peut être filtré par date, par source (titre de presse), et par emplacement selon les paramètres.

Filtre par emplacement : pour chaque article, si il ne contenient pas un des lieux d'intérêts dans son champs 'loc'
définit à l'étape 1, il n'est pas conservé. 

Filtre par source : si le code de la source source_code (par exemple YONREP pour l'Yonne Républcaine) n'est pas contenu dans le chemin de 
l'article, il n'est pas conservé. 

Filtre par date : il est aussi basé sur le chemin où a été sauvé l'article, définit à l'étape 1. 

Pour rappel, à la fin de l'étape 1, les articles sont stockés de la manière suivante : 

output_path/source_code/année_de_publication/mois_de_publication/article_code.

## Pour commencer

### Prérequis

Créer un environement virtuel et l'activer. 

```
sudo pip install virtualenv
virtualenv venv
source venv/bin/activate
```

Installer les requierements. 

```
pip install -r requierements.txt
```

### Remplacer les chemins par défaut

Dans config/path.py remplacer les chemins par les chemins correspondant sur votre ordinateur.

output_path : là où les articles mis à jour seront sauvés. 

path_test_articles : les articles utilisés dans les tests unitaires. 

path_input_data : là où sont lu les articles. 

path_output_data : là où sont stockés les articles mis à jour. 

* path_input doit être identique à path_output de l'étape 1, et path_output de l'étape 2 doit être identique à path_input de l'étape 3.*


### Lire le corpus en entrée

écrire dans le terminal : 

```
python main.py
```


Cela effectue uniquement un filtre sur les articles tagués sport par défaut.

Les articles sont lus depuis path_article_base from config/path.py, et sauvés dans path_output from config/path.py.

*Attention : path_article_base doit être identique à path_output de l'étape 1, et path_output de l'étape 2 doit être identique à path_input de l'étape 3.*



Pour ajouter des filtres : 

* par date : modifier date_min_str et date_max_str dans le main. Le format nécessaire est 'année/mois' ;

* par sources : en paramètre dans la ligne de commande avec -src. Le noms des sources est celui au format standard fournit dans etape_1/config/config_files/DJ_Sources_Titles_Keys_v2.csv [ici](https://bitbucket.org/Marjolaine_Grunenberger/etape_1/src/7ec17780db4a610ae87f4a988e0a468f92214690/config/config_files/?at=master), dans la colone LMP Source Code ; 

exemple : 
```
python main.py -src TFHOUR,EQUIPM

```

permet d'avoir les articles des sources 24 Heures et L'Equipe. 

* par emplacement : en paramètre dans la ligne de commande. Si on le passe en paramètre, les emplacements conservés sont ceux indiqués dans le fichier input/list_locations_of_interest.txt ;

exemple : 
```
python main.py -loc

```

gardera les personnes ayant une co-occurrence avec un des lieux d'intérêt contenu dans list_loc_of_interest et considérera les contextes comme ' cleaned contextes' si ils apparaissent dans des articles ayant une occurrence avec un des lien d'intérêt (les cleaned_articles). 

* par sujet : remplir les champs lst_of_wrds_to_exclude  et lst_of_wrds_to_include dans le main. lst_of_wrds_to_exclude est une liste de regex qui permettent de ne pas prendre en compte des faux positifs dans la recherche de mots clés. Par exemple on ne veut pas 'tombe à l'eau'si l'on cherche des articles liés à l'eau. lst_of_wrds_to_include est une liste de mots d'intérêt.

* remove_sport : si est vrai dans le main, on ne garde pas les articles tagués sport. Sinon le modifier dans le main. 

